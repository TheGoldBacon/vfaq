package me.bacon.plugin;

import java.util.logging.Logger;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin implements Listener{
    Logger logger = getLogger();
    public static Main plugin;

    public void onDisable() {
        PluginDescriptionFile pdfFile = this.getDescription();
        this.logger.info(pdfFile.getName() + " Has been disabled!");
    }

    public void onEnable() {
        plugin = this;
        PluginDescriptionFile pdfFile = this.getDescription();
        this.logger.info(pdfFile.getName() + " Version: "
                + pdfFile.getVersion() + " Has been Enabled!");
        PluginManager pm = getServer().getPluginManager();
        pm.registerEvents(this, this);
        getConfig().options().copyDefaults(true);
        saveConfig();
    }

    public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args){
        Player player = (Player) sender;
        if(commandLabel.equalsIgnoreCase("faq")){ //reloads the config of the plugin
            if(player.isOp()){
                if(args.length == 0){
                    player.sendMessage(ChatColor.RED + "Invalid args! /faq reload");
                }else if(args[1].equalsIgnoreCase("reload")){
                    reloadConfig();
                    prefix = getConfig().getString("Prefix");
                    player.sendMessage( prefix + ChatColor.GREEN + "Config Reloaded and Saved!");
                }
            } else{
                player.sendMessage(prefix + ChatColor.RED + "No Permission!");
            }
        }
        return false;
    }

    String prefix = getConfig().getString("Prefix");

    @EventHandler
    public void onChat(AsyncPlayerChatEvent e){
        Player player = e.getPlayer();
        if(player.hasPermission("faq.use")){
            String[] args = e.getMessage().split(" ");
            if(args[0].equalsIgnoreCase("!faq")){
                player.sendMessage(ChatColor.RED + "Invalid Args!" + ChatColor.DARK_RED + " !faq <String>");
                final ConfigurationSection commands = getConfig().getConfigurationSection("Commands");
                for(final String key : commands.getKeys(false)){
                    if(args[1].equalsIgnoreCase(key)){
                        Bukkit.getScheduler().scheduleSyncDelayedTask(this, new Runnable() {
                            public void run() {
                                for (String s : commands.getStringList(key)){
                                    Bukkit.broadcastMessage(ChatColor.translateAlternateColorCodes('&', prefix + " " + s));
                                }
                            }
                        }, 1l);
                    }
                }
            }
        }
    }
}

